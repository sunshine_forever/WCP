<h2>V2.2.1---：</h2>
<pre>

1.后台文档管理-阅读权限中“禁止阅读”改为“禁止”

2.推荐小组中不应包含已加入的小组

3.推荐小组量小于3个不显示分页

4.阅读帖子后，点击分类名，页面跳转到分类目录

5.新建文档时设置默认权限为个人编辑公开阅读

6.修正若干bug

7.超文本编辑器支持插入知识引用

8.小组描述允许字符数量扩展到1024

9.允许维护小组首页(FARM_DOCGROUP表中增加字段：HOMEDOCID varchar(32))

</pre>

<hr/>
<h1>QQ:WCP技术交流群 422838195</h1>
<h1>演示环境:<a href="http://123.56.101.90:8999">123.56.101.90:8999</a> </h1>
<p>
本项目的应用场景是管理技术团队的相关知识（API、代码片段、知识定义、技术经验...）
但是其应用并不局限于这些应用，当然你最好下载一个安装版先试一试。其实这就是一个
知识库、知识管理系统、以及若干基于知识的工具和视图。
<br/>
<img src='http://static.oschina.net/uploads/space/2014/1215/151810_Dhd6_102590.png'/>
<br/>

### 1.        系统前台

（支持手机端访问、另有后台管理提供高级功能）

### 2.        内容发布展示

（支持版本管理、目录章节管理、附件管理、源代码发布、文档好评度管理、全文检索）


### 3.        技术网站整站下载为平台格式：

（支持整站下载、单个网页下载、对下载内容通过配置的方式抽取有用数据）


### 4.        权限划分

文档访问权限和编辑权限可以分别控制：

 [高级权限|普通权限]  X  [小组|创建人|公开]  X  [系统中N个小组]  X  [阅读|编辑]  
 = 12N种权限组合


### 5.        统计分析

（用户好评度、小组好评度、用户发布量、文章好评、文章差评、系统日使用量、月使用量、
年使用量、文档好评率、文档访问量、用户累计好评、用户累计差评、用户等级等统计项）